package com.developer.fbenutti.barsawars;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.Image;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.developer.fbenutti.barsawars.util.Util;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

public class MainActivity extends AppCompatActivity {

    private TextView tvBemvindo;
    private TextView tvSelecione;
    private TextView tvPessoas;
    private TextView tvFilmes;
    private TextView tvEspecies;
    private TextView tvPlanetas;
    private TextView tvNaves;
    private TextView tvVeiculos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvBemvindo = (TextView) findViewById(R.id.tvBemvindo);
        tvSelecione = (TextView) findViewById(R.id.tvSelecione);
        tvPessoas = (TextView) findViewById(R.id.tvPessoas);
        tvFilmes = (TextView) findViewById(R.id.tvFilmes);
        tvEspecies = (TextView) findViewById(R.id.tvEspecies);
        tvPlanetas = (TextView) findViewById(R.id.tvPlanetas);
        tvNaves = (TextView) findViewById(R.id.tvNaves);
        tvVeiculos = (TextView) findViewById(R.id.tvVeiculos);

        Typeface face = Typeface.createFromAsset(getAssets(),
                "FranklinGothicTM.ttf");

        tvBemvindo.setTypeface(face);
        tvSelecione.setTypeface(face);
        tvPessoas.setTypeface(face);
        tvFilmes.setTypeface(face);
        tvEspecies.setTypeface(face);
        tvPlanetas.setTypeface(face);
        tvNaves.setTypeface(face);
        tvVeiculos.setTypeface(face);

        final MediaPlayer lightSaberMP = MediaPlayer.create(this, R.raw.lightsaberon);

        ImageButton ibFilmes = (ImageButton) this.findViewById(R.id.ibFilmes);
        ImageButton ibPessoas = (ImageButton) this.findViewById(R.id.ibPessoas);
        ImageButton ibEspecies = (ImageButton) this.findViewById(R.id.ibEspecies);
        ImageButton ibPlanetas = (ImageButton) this.findViewById(R.id.ibPlanetas);
        ImageButton ibNaves = (ImageButton) this.findViewById(R.id.ibNaves);
        ImageButton ibVeiculos = (ImageButton) this.findViewById(R.id.ibVeiculos);

        ibFilmes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lightSaberMP.start();
                listaFilmes(view);
            }
        });

        ibPessoas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lightSaberMP.start();
                listaPessoas(view);
            }
        });

        ibEspecies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lightSaberMP.start();
                listaEspecies(view);
            }
        });

        ibPlanetas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lightSaberMP.start();
                listaPlanetas(view);
            }
        });

        ibNaves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lightSaberMP.start();
                listaNaves(view);
            }
        });

        ibVeiculos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lightSaberMP.start();
                listaVeiculos(view);
            }
        });

    }

    public void listaFilmes(View v){
        Intent intent = new Intent(this, FilmesActivity.class);
        startActivity(intent);
    }

    public void listaPessoas(View v){
        Intent intent = new Intent(this, PessoasActivity.class);
        startActivity(intent);
    }

    public void listaEspecies(View v){
        Intent intent = new Intent(this, EspeciesActivity.class);
        startActivity(intent);
    }

    public void listaPlanetas(View v){
        Intent intent = new Intent(this, PlanetasActivity.class);
        startActivity(intent);
    }

    public void listaNaves(View v){
        Intent intent = new Intent(this, NavesActivity.class);
        startActivity(intent);
    }

    public void listaVeiculos(View v){
        Intent intent = new Intent(this, VeiculosActivity.class);
        startActivity(intent);
    }
}