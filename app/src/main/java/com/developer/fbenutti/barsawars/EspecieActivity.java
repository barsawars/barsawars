package com.developer.fbenutti.barsawars;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

import com.developer.fbenutti.barsawars.modelo.Species;

public class EspecieActivity extends AppCompatActivity {
    private TextView tvNome;
    private TextView tvClassificacao;
    private TextView tvTamanhoMedio;
    private TextView tvTempoVidaMedio;
    private TextView tvCorOlho;
    private TextView tvCorCabelo;
    private TextView tvCorPele;
    private TextView tvIdioma;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_especie);
        tvNome = (TextView) findViewById(R.id.tvNome);
        tvClassificacao = (TextView) findViewById(R.id.txtRespostaClassificacao);
        tvTamanhoMedio = (TextView) findViewById(R.id.txtRespostaTamanhoMedio);
        tvTempoVidaMedio = (TextView) findViewById(R.id.txtRespostaTempoVidaMedio);
        tvIdioma = (TextView) findViewById(R.id.txtRespostaIdioma);
        tvCorPele = (TextView) findViewById(R.id.txtRespostaCorPele);
        tvCorCabelo = (TextView) findViewById(R.id.txtRespostaCorCabelo);
        tvCorOlho = (TextView) findViewById(R.id.txtRespostaCorOlho);

        Species f = (Species) getIntent().getSerializableExtra("especie");

        if( f != null)
        {
            preecherDados(f);
        }
        else{
            Toast.makeText(this, "Sua contagem de midichlorians está baixa. " +
                    "Treine mais e tente novamente!", Toast.LENGTH_LONG).show();
        }
    }

    private void preecherDados(Species especie) {

        tvNome.setText( especie.getName() );
        tvClassificacao.setText( especie.getClassification() );
        tvTamanhoMedio.setText( especie.getAverageHeight() );
        tvTempoVidaMedio.setText( especie.getAverageLifespan() );
        tvIdioma.setText( especie.getLanguage() );
        tvCorOlho.setText( especie.getEyeColors() );
        tvCorCabelo.setText( especie.getHairColors() );
        tvCorPele.setText( especie.getSkinColors() );

        final Species sp = especie;
        final Handler textViewHandler = new Handler();

        new AsyncTask<Void, Void, String>() {
            private ProgressDialog progressDialog = new ProgressDialog(
                    EspecieActivity.this, ProgressDialog.THEME_HOLO_DARK);

            @Override
            protected void onPreExecute() {
                progressDialog.setTitle("Tenha calma, jovem Padawan");
                progressDialog.setMessage("A força está agindo...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
            }
            @Override
            protected String doInBackground(Void... params) {
                try{
                    TranslateOptions options = TranslateOptions.newBuilder()
                            .setApiKey("AIzaSyBAJ7OKIVOLG5hSUV7DwlL6Is_9s1D_KaQ")
                            .build();
                    Translate translate = options.getService();
                    final Translation translationNome =
                            translate.translate(sp.getName(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationClassificacao =
                            translate.translate(sp.getClassification(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationTamanhoMedio =
                            translate.translate(sp.getAverageHeight(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationTempoVidaMedio =
                            translate.translate(sp.getAverageLifespan(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationIdioma =
                            translate.translate(sp.getLanguage(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationCorOlho =
                            translate.translate(sp.getEyeColors(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationCorCabelo =
                            translate.translate(sp.getHairColors(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationCorPele =
                            translate.translate(sp.getSkinColors(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    textViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            tvNome.setText(translationNome.getTranslatedText());
                            tvClassificacao.setText(translationClassificacao.getTranslatedText());
                            tvTamanhoMedio.setText(translationTamanhoMedio.getTranslatedText());
                            tvTempoVidaMedio.setText(translationTempoVidaMedio.getTranslatedText());
                            tvIdioma.setText(translationIdioma.getTranslatedText());
                            tvCorOlho.setText(translationCorOlho.getTranslatedText());
                            tvCorCabelo.setText(translationCorCabelo.getTranslatedText());
                            tvCorPele.setText(translationCorPele.getTranslatedText());
                        }
                    });

                }
                catch (Exception ex){
                    return ex.toString();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String msg) {
                progressDialog.dismiss();
                if (msg != null) {
                    if (!msg.isEmpty()) {
                        Toast.makeText(context, "Não foi possível realizar a tradução!"
                                , Toast.LENGTH_LONG).show();
                    }
                }
            }
        }.execute();
    }

}
