package com.developer.fbenutti.barsawars;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.developer.fbenutti.barsawars.modelo.People;
import com.developer.fbenutti.barsawars.modelo.SWModelList;
import com.developer.fbenutti.barsawars.sw.StarWars;
import com.developer.fbenutti.barsawars.sw.StarWarsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PessoasActivity extends AppCompatActivity {

    private People f;
    private List<People> Pessoas;
    private ListView lvPessoas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pessoas);

        Pessoas = new ArrayList<People>();
        lvPessoas = (ListView) findViewById(R.id.lv);

        StarWarsApi.init();
        StarWars api = StarWarsApi.getApi();

        api.getAllPeople(1, new Callback<SWModelList<People>>() {

            @Override
            public void success(SWModelList<People> peopleSWModelList, Response response) {
                Pessoas = peopleSWModelList.results;

                if (Pessoas != null) {
                    if (Pessoas.size() > 0) {
                        PessoaListAdapter la = new PessoaListAdapter(
                                getApplicationContext(), Pessoas);
                        lvPessoas.setAdapter(la);
                    }
                }
            }

            public void failure(RetrofitError error) {

            }
        });

        //Seta o listener para a selecao com click simples
        lvPessoas.setOnItemClickListener(selecionar);
    }

    private AdapterView.OnItemClickListener selecionar = new AdapterView.OnItemClickListener() {

        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) {
            f = Pessoas.get(pos);
            Intent i = new Intent(arg1.getContext(), PessoaActivity.class);
            i.putExtra("pessoa", f);
            startActivity(i);
        }

    };

}
