package com.developer.fbenutti.barsawars;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.developer.fbenutti.barsawars.modelo.Species;
import com.developer.fbenutti.barsawars.modelo.SWModelList;
import com.developer.fbenutti.barsawars.sw.StarWars;
import com.developer.fbenutti.barsawars.sw.StarWarsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class EspeciesActivity extends AppCompatActivity {

    private Species f;
    private List<Species> Especies;
    private ListView lvEspecies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_especies);

        Especies = new ArrayList<Species>();
        lvEspecies = (ListView) findViewById(R.id.lv);

        StarWarsApi.init();
        StarWars api = StarWarsApi.getApi();

        api.getAllSpecies(1, new Callback<SWModelList<Species>>() {

            @Override
            public void success(SWModelList<Species> speciesSWModelList, Response response) {
                Especies = speciesSWModelList.results;

                System.out.println("Quantidade de Especies: "+ Especies.size());

                if (Especies != null) {
                    if (Especies.size() > 0) {
                        EspecieListAdapter la = new EspecieListAdapter(
                                getApplicationContext(), Especies);
                        lvEspecies.setAdapter(la);
                    }
                }
            }

            public void failure(RetrofitError error) {

            }
        });

        //Seta o listener para a selecao com click simples
        lvEspecies.setOnItemClickListener(selecionar);
    }

    private AdapterView.OnItemClickListener selecionar = new AdapterView.OnItemClickListener() {

        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) {
            f = Especies.get(pos);
            Intent i = new Intent(arg1.getContext(), EspecieActivity.class);
            i.putExtra("especie", f);
            startActivity(i);
        }

    };

}
