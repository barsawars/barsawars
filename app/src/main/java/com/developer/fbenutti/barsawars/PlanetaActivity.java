package com.developer.fbenutti.barsawars;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.fbenutti.barsawars.modelo.Planet;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

public class PlanetaActivity extends AppCompatActivity {

    private TextView tvNome;
    private TextView tvPeriodoRotacao;
    private TextView tvPeriodoOrbital;
    private TextView tvDiametro;
    private TextView tvClima;
    private TextView tvGravidade;
    private TextView tvTerreno;
    private TextView tvPopulacao;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planeta);
        tvNome = (TextView) findViewById(R.id.tvTitulo);
        tvPeriodoRotacao = (TextView) findViewById(R.id.txtRespostaPeriodoRotacao);
        tvPeriodoOrbital = (TextView) findViewById(R.id.txtRespostaPeriodoOrbital);
        tvDiametro = (TextView) findViewById(R.id.txtRespostaDiametro);
        tvClima = (TextView) findViewById(R.id.txtRespostaClima);
        tvGravidade = (TextView) findViewById(R.id.txtRespostaGravidade);
        tvTerreno = (TextView) findViewById(R.id.txtRespostaTerreno);
        tvPopulacao = (TextView) findViewById(R.id.txtRespostaPopulacao);

        Planet f = (Planet) getIntent().getSerializableExtra("planeta");

        if( f != null)
        {
            preecherDados(f);
        }
        else{
            Toast.makeText(this, "Sua contagem de midichlorians está baixa. " +
                    "Treine mais e tente novamente!", Toast.LENGTH_LONG).show();
        }
    }

    private void preecherDados(Planet p) {
        tvNome.setText(p.getName());
        tvPeriodoRotacao.setText(p.getRotationPeriod());
        tvPeriodoOrbital.setText(p.getOrbitalPeriod());
        tvDiametro.setText(p.getDiameter());
        tvClima.setText(p.getClimate());
        tvGravidade.setText(p.getGravity());
        tvTerreno.setText(p.getTerrain());
        tvPopulacao.setText(p.getPopulation());

        final Planet planet = p;
        final Handler textViewHandler = new Handler();

        new AsyncTask<Void, Void, String>() {
            private ProgressDialog progressDialog = new ProgressDialog(
                    PlanetaActivity.this, ProgressDialog.THEME_HOLO_DARK);

            @Override
            protected void onPreExecute() {
                progressDialog.setTitle("Tenha calma, jovem Padawan");
                progressDialog.setMessage("A força está agindo...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
            }
            @Override
            protected String doInBackground(Void... params) {
                try{
                    TranslateOptions options = TranslateOptions.newBuilder()
                            .setApiKey("AIzaSyBAJ7OKIVOLG5hSUV7DwlL6Is_9s1D_KaQ")
                            .build();
                    Translate translate = options.getService();
                    final Translation translationNome =
                            translate.translate(planet.getName(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationClima =
                            translate.translate(planet.getClimate(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationGravidade =
                            translate.translate(planet.getGravity(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationTerreno =
                            translate.translate(planet.getTerrain(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationPopulacao =
                            translate.translate(planet.getPopulation(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    textViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            tvNome.setText(translationNome.getTranslatedText());
                            tvClima.setText(translationClima.getTranslatedText());
                            tvGravidade.setText(translationGravidade.getTranslatedText());
                            tvTerreno.setText(translationTerreno.getTranslatedText());
                            tvPopulacao.setText(translationPopulacao.getTranslatedText());
                        }
                    });

                }
                catch (Exception ex){
                    return ex.toString();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String msg) {
                progressDialog.dismiss();
                if (msg != null) {
                    if (!msg.isEmpty()) {
                        Toast.makeText(context, "Não foi possível realizar a tradução!"
                                , Toast.LENGTH_LONG).show();
                    }
                }
            }
        }.execute();

    }

}
