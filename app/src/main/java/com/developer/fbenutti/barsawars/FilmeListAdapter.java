package com.developer.fbenutti.barsawars;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.developer.fbenutti.barsawars.modelo.Film;

import java.util.List;

public class FilmeListAdapter extends BaseAdapter {

	private Context context;
	private List<Film> lista;
	
	public FilmeListAdapter(Context context, List<Film> filmes){
		this.context = context;
		this.lista = filmes;
	}
	
	@Override
	public int getCount() {
		return this.lista.size();
	}

	@Override
	public Object getItem(int posicao) {
		return this.lista.get(posicao);
	}

	@Override
	public long getItemId(int posicao) {
		return posicao;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Film f = lista.get(position);
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.filmes, null);
		
		TextView nome = (TextView) view.findViewById(R.id.txtRespostaNome);
		nome.setText(f.getTitle());
		
		TextView dataEstreia = (TextView) view.findViewById(R.id.txtRespostaDataEstreia);
		dataEstreia.setText(String.valueOf(f.getReleaseDate()));
		
		return view;
	}

}
