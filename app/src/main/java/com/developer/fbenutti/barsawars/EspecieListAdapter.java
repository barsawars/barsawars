package com.developer.fbenutti.barsawars;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.developer.fbenutti.barsawars.modelo.Species;

import java.util.List;

public class EspecieListAdapter extends BaseAdapter {

	private Context context;
	private List<Species> lista;

	public EspecieListAdapter(Context context, List<Species> especies){
		this.context = context;
		this.lista = especies;
	}
	
	@Override
	public int getCount() {
		return this.lista.size();
	}

	@Override
	public Object getItem(int posicao) {
		return this.lista.get(posicao);
	}

	@Override
	public long getItemId(int posicao) {
		return posicao;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Species f = lista.get(position);
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.especies, null);
		
		TextView nome = (TextView) view.findViewById(R.id.txtRespostaNome);
		nome.setText(f.getName());
		
		TextView classificacao = (TextView) view.findViewById(R.id.txtRespostaClassificacao);
		classificacao.setText(String.valueOf(f.getClassification()));
		
		return view;
	}

}
