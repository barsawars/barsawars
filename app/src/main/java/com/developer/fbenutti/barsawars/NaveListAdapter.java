package com.developer.fbenutti.barsawars;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.developer.fbenutti.barsawars.modelo.Starship;

import java.util.List;

public class NaveListAdapter extends BaseAdapter {

	private Context context;
	private List<Starship> lista;

	public NaveListAdapter(Context context, List<Starship> naves){
		this.context = context;
		this.lista = naves;
	}
	
	@Override
	public int getCount() {
		return this.lista.size();
	}

	@Override
	public Object getItem(int posicao) {
		return this.lista.get(posicao);
	}

	@Override
	public long getItemId(int posicao) {
		return posicao;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Starship f = lista.get(position);
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.naves, null);
		
		TextView nome = (TextView) view.findViewById(R.id.txtRespostaNome);
		nome.setText(f.getName());
		
		TextView genero = (TextView) view.findViewById(R.id.txtRespostaModelo);
		genero.setText(String.valueOf(f.getModel()));
		
		return view;
	}

}
