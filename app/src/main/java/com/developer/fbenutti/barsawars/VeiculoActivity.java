package com.developer.fbenutti.barsawars;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.fbenutti.barsawars.modelo.Vehicle;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

public class VeiculoActivity extends AppCompatActivity {

    private TextView tvNome;
    private TextView tvModelo;
    private TextView tvFabricante;
    private TextView tvValor;
    private TextView tvComprimento;
    private TextView tvVelocidadeAtmosferica;
    private TextView tvTripulacao;
    private TextView tvPassageiros;
    private TextView tvCapacidadeCarga;
    private TextView tvConsumiveis;
    private TextView tvClassificacaoHyperdrive;
    private TextView tvClasse;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_veiculo);
        tvNome = (TextView) findViewById(R.id.tvTitulo);
        tvModelo = (TextView) findViewById(R.id.txtRespostaModelo);
        tvFabricante = (TextView) findViewById(R.id.txtRespostaFabricante);
        tvValor = (TextView) findViewById(R.id.txtRespostaValor);
        tvComprimento = (TextView) findViewById(R.id.txtRespostaComprimento);
        tvVelocidadeAtmosferica = (TextView) findViewById(R.id.txtRespostaVelocidadeAtmosferica);
        tvTripulacao = (TextView) findViewById(R.id.txtRespostaTripulacao);
        tvPassageiros = (TextView) findViewById(R.id.txtRespostaPassageiros);
        tvCapacidadeCarga = (TextView) findViewById(R.id.txtRespostaCapacidadeCarga);
        tvConsumiveis = (TextView) findViewById(R.id.txtRespostaConsumiveis);
        tvClassificacaoHyperdrive = (TextView) findViewById(R.id.txtRespostaClassificacaoHyperdrive);
        tvClasse = (TextView) findViewById(R.id.txtRespostaClasse);

        Vehicle f = (Vehicle) getIntent().getSerializableExtra("veiculo");

        if( f != null)
        {
            preecherDados(f);
        }
        else{
            Toast.makeText(this, "Sua contagem de midichlorians está baixa. " +
                    "Treine mais e tente novamente!", Toast.LENGTH_LONG).show();
        }
    }

    private void preecherDados(Vehicle veiculo) {
        tvNome.setText(veiculo.getName());
        tvModelo.setText(veiculo.getModel());
        tvFabricante.setText(veiculo.getManufacturer());
        tvValor.setText(veiculo.getCostInCredits());
        tvComprimento.setText(veiculo.getLength());
        tvVelocidadeAtmosferica.setText(veiculo.getMaxAtmospheringSpeed());
        tvTripulacao.setText(veiculo.getCrew());
        tvPassageiros.setText(veiculo.getPassengers());
        tvCapacidadeCarga.setText(veiculo.getCargoCapacity());
        tvConsumiveis.setText(veiculo.getConsumables());
        tvClasse.setText(veiculo.getVehicleClass());

        final Vehicle n = veiculo;
        final Handler textViewHandler = new Handler();

        new AsyncTask<Void, Void, String>() {
            private ProgressDialog progressDialog = new ProgressDialog(
                    VeiculoActivity.this, ProgressDialog.THEME_HOLO_DARK);

            @Override
            protected void onPreExecute() {
                progressDialog.setTitle("Tenha calma, jovem Padawan");
                progressDialog.setMessage("A força está agindo...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
            }
            @Override
            protected String doInBackground(Void... params) {
                try{
                    TranslateOptions options = TranslateOptions.newBuilder()
                            .setApiKey("AIzaSyBAJ7OKIVOLG5hSUV7DwlL6Is_9s1D_KaQ")
                            .build();
                    Translate translate = options.getService();
                    final Translation translationNome =
                            translate.translate(n.getName(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationModelo =
                            translate.translate(n.getModel(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationFabricante =
                            translate.translate(n.getManufacturer(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationConsumiveis =
                            translate.translate(n.getConsumables(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationClasse =
                            translate.translate(n.getVehicleClass(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    textViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            tvNome.setText(translationNome.getTranslatedText());
                            tvModelo.setText(translationModelo.getTranslatedText());
                            tvFabricante.setText(translationFabricante.getTranslatedText());
                            tvConsumiveis.setText(translationConsumiveis.getTranslatedText());
                            tvClasse.setText(translationClasse.getTranslatedText());
                        }
                    });

                }
                catch (Exception ex){
                    return ex.toString();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String msg) {
                progressDialog.dismiss();
                if (msg != null) {
                    if (!msg.isEmpty()) {
                        Toast.makeText(context, "Não foi possível realizar a tradução!"
                                , Toast.LENGTH_LONG).show();
                    }
                }
            }
        }.execute();

    }

}
