package com.developer.fbenutti.barsawars;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.developer.fbenutti.barsawars.modelo.Planet;
import com.developer.fbenutti.barsawars.modelo.SWModelList;
import com.developer.fbenutti.barsawars.sw.StarWars;
import com.developer.fbenutti.barsawars.sw.StarWarsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PlanetasActivity extends AppCompatActivity {

    private Planet f;
    private List<Planet> Planetas;
    private ListView lvPlanetas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planetas);

        Planetas = new ArrayList<Planet>();
        lvPlanetas = (ListView) findViewById(R.id.lv);

        StarWarsApi.init();
        StarWars api = StarWarsApi.getApi();

        api.getAllPlanets(1, new Callback<SWModelList<Planet>>() {

            @Override
            public void success(SWModelList<Planet> planetSWModelList, Response response) {
                Planetas = planetSWModelList.results;

                if (Planetas != null) {
                    if (Planetas.size() > 0) {
                        PlanetaListAdapter la = new PlanetaListAdapter(
                                getApplicationContext(), Planetas);
                        lvPlanetas.setAdapter(la);
                    }
                }
            }

            public void failure(RetrofitError error) {

            }
        });

        //Seta o listener para a selecao com click simples
        lvPlanetas.setOnItemClickListener(selecionar);
    }

    private AdapterView.OnItemClickListener selecionar = new AdapterView.OnItemClickListener() {

        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) {
            f = Planetas.get(pos);
            Intent i = new Intent(arg1.getContext(), PlanetaActivity.class);
            i.putExtra("planeta", f);
            startActivity(i);
        }

    };

}
