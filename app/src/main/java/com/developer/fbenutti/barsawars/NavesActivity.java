package com.developer.fbenutti.barsawars;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.developer.fbenutti.barsawars.modelo.Starship;
import com.developer.fbenutti.barsawars.modelo.SWModelList;
import com.developer.fbenutti.barsawars.sw.StarWars;
import com.developer.fbenutti.barsawars.sw.StarWarsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class NavesActivity extends AppCompatActivity {

    private Starship f;
    private List<Starship> Naves;
    private ListView lvNaves;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_naves);

        Naves = new ArrayList<Starship>();
        lvNaves = (ListView) findViewById(R.id.lv);

        StarWarsApi.init();
        StarWars api = StarWarsApi.getApi();

        api.getAllStarships(1, new Callback<SWModelList<Starship>>() {

            @Override
            public void success(SWModelList<Starship> starshipSWModelList, Response response) {
                Naves = starshipSWModelList.results;

                if (Naves != null) {
                    if (Naves.size() > 0) {
                        NaveListAdapter la = new NaveListAdapter(
                                getApplicationContext(), Naves);
                        lvNaves.setAdapter(la);
                    }
                }
            }

            public void failure(RetrofitError error) {

            }
        });

        //Seta o listener para a selecao com click simples
        lvNaves.setOnItemClickListener(selecionar);
    }

    private AdapterView.OnItemClickListener selecionar = new AdapterView.OnItemClickListener() {

        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) {
            f = Naves.get(pos);
            Intent i = new Intent(arg1.getContext(), NaveActivity.class);
            i.putExtra("nave", f);
            startActivity(i);
        }

    };

}
