package com.developer.fbenutti.barsawars;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.fbenutti.barsawars.modelo.Film;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

public class FilmeActivity extends AppCompatActivity {

    private TextView tvTitulo;
    private TextView tvEpisodio;
    private TextView tvDataEstreia;
    private TextView tvDiretor;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filme);
        tvTitulo = (TextView) findViewById(R.id.tvTitulo);
        tvEpisodio = (TextView) findViewById(R.id.txtRespostaEpisodio);
        tvDataEstreia = (TextView) findViewById(R.id.txtRespostaDataEstreia);
        tvDiretor = (TextView) findViewById(R.id.txtRespostaDiretor);

        Film f = (Film) getIntent().getSerializableExtra("filme");

        if( f != null)
        {
            preecherDados(f);
        }
        else{
            Toast.makeText(this, "Sua contagem de midichlorians está baixa. " +
                    "Treine mais e tente novamente!", Toast.LENGTH_LONG).show();
        }

    }

    private void preecherDados(Film Filme) {
        tvTitulo.setText(Filme.getTitle());
        tvEpisodio.setText(String.valueOf(Filme.getEpisodeId()));
        tvDataEstreia.setText(Filme.getReleaseDate());
        tvDiretor.setText(Filme.getDirector());

        final Film f = Filme;
        final Handler textViewHandler = new Handler();

        new AsyncTask<Void, Void, String>() {
            private ProgressDialog progressDialog = new ProgressDialog(
                    FilmeActivity.this, ProgressDialog.THEME_HOLO_DARK);

            @Override
            protected void onPreExecute() {
                progressDialog.setTitle("Tenha calma, jovem Padawan");
                progressDialog.setMessage("A força está agindo...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
            }
            @Override
            protected String doInBackground(Void... params) {
                try{
                    TranslateOptions options = TranslateOptions.newBuilder()
                            .setApiKey("AIzaSyBAJ7OKIVOLG5hSUV7DwlL6Is_9s1D_KaQ")
                            .build();
                    Translate translate = options.getService();
                    final Translation translationTitulo =
                            translate.translate(f.getTitle(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationDiretor =
                            translate.translate(f.getDirector(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    textViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            tvTitulo.setText(translationTitulo.getTranslatedText());
                            tvDiretor.setText(translationDiretor.getTranslatedText());
                        }
                    });

                }
                catch (Exception ex){
                    return ex.toString();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String msg) {
                progressDialog.dismiss();
                if (msg != null) {
                    if (!msg.isEmpty()) {
                        Toast.makeText(context, "Não foi possível realizar a tradução!"
                                , Toast.LENGTH_LONG).show();
                    }
                }
            }
        }.execute();
        
    }

}
