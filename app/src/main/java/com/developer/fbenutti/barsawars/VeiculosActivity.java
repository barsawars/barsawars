package com.developer.fbenutti.barsawars;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.developer.fbenutti.barsawars.modelo.SWModelList;
import com.developer.fbenutti.barsawars.modelo.Vehicle;
import com.developer.fbenutti.barsawars.sw.StarWars;
import com.developer.fbenutti.barsawars.sw.StarWarsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class VeiculosActivity extends AppCompatActivity {

    private Vehicle f;
    private List<Vehicle> Veiculos;
    private ListView lvVeiculos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_veiculos);

        Veiculos = new ArrayList<Vehicle>();
        lvVeiculos = (ListView) findViewById(R.id.lv);

        StarWarsApi.init();
        StarWars api = StarWarsApi.getApi();

        api.getAllVehicles(1, new Callback<SWModelList<Vehicle>>() {

            @Override
            public void success(SWModelList<Vehicle> vehicleSWModelList, Response response) {
                Veiculos = vehicleSWModelList.results;

                if (Veiculos != null) {
                    if (Veiculos.size() > 0) {
                        VeiculoListAdapter la = new VeiculoListAdapter(
                                getApplicationContext(), Veiculos);
                        lvVeiculos.setAdapter(la);
                    }
                }
            }

            public void failure(RetrofitError error) {

            }
        });

        //Seta o listener para a selecao com click simples
        lvVeiculos.setOnItemClickListener(selecionar);
    }

    private AdapterView.OnItemClickListener selecionar = new AdapterView.OnItemClickListener() {

        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) {
            f = Veiculos.get(pos);
            Intent i = new Intent(arg1.getContext(), VeiculoActivity.class);
            i.putExtra("veiculo", f);
            startActivity(i);
        }

    };

}
