package com.developer.fbenutti.barsawars;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.fbenutti.barsawars.modelo.People;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

public class PessoaActivity extends AppCompatActivity {

    private TextView tvNome;
    private TextView tvAnoNascimento;
    private TextView tvCorOlho;
    private TextView tvGenero;
    private TextView tvCorCabelo;
    private TextView tvAltura;
    private TextView tvPeso;
    private TextView tvCorPele;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pessoa);
        tvNome = (TextView) findViewById(R.id.tvTitulo);
        tvAnoNascimento = (TextView) findViewById(R.id.txtRespostaAnoNascimento);
        tvCorOlho = (TextView) findViewById(R.id.txtRespostaCorOlho);
        tvGenero = (TextView) findViewById(R.id.txtRespostaGenero);
        tvCorCabelo = (TextView) findViewById(R.id.txtRespostaCorCabelo);
        tvAltura = (TextView) findViewById(R.id.txtRespostaAltura);
        tvPeso = (TextView) findViewById(R.id.txtRespostaPeso);
        tvCorPele = (TextView) findViewById(R.id.txtRespostaCorPele);

        People f = (People) getIntent().getSerializableExtra("pessoa");

        if( f != null)
        {
            preecherDados(f);
        }
        else{
            Toast.makeText(this, "Sua contagem de midichlorians está baixa. " +
                    "Treine mais e tente novamente!", Toast.LENGTH_LONG).show();
        }
    }

    private void preecherDados(People pessoa) {
        tvNome.setText(pessoa.getName());
        tvAnoNascimento.setText(pessoa.getBirthYear());
        tvGenero.setText(pessoa.getGender());
        tvAltura.setText(pessoa.getHeight());
        tvPeso.setText(pessoa.getMass());
        tvCorOlho.setText(pessoa.getEyeColor());
        tvCorCabelo.setText(pessoa.getHairColor());
        tvCorPele.setText(pessoa.getSkinColor());

        final People sp = pessoa;
        final Handler textViewHandler = new Handler();

        new AsyncTask<Void, Void, String>() {
            private ProgressDialog progressDialog = new ProgressDialog(
                    PessoaActivity.this, ProgressDialog.THEME_HOLO_DARK);

            @Override
            protected void onPreExecute() {
                progressDialog.setTitle("Tenha calma, jovem Padawan");
                progressDialog.setMessage("A força está agindo...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
            }
            @Override
            protected String doInBackground(Void... params) {
                try{
                    TranslateOptions options = TranslateOptions.newBuilder()
                            .setApiKey("AIzaSyBAJ7OKIVOLG5hSUV7DwlL6Is_9s1D_KaQ")
                            .build();
                    Translate translate = options.getService();
                    final Translation translationNome =
                            translate.translate(sp.getName(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationGenero =
                            translate.translate(sp.getGender(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationCorOlho =
                            translate.translate(sp.getEyeColor(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationCorCabelo =
                            translate.translate(sp.getHairColor(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    final Translation translationCorPele =
                            translate.translate(sp.getSkinColor(),
                                    Translate.TranslateOption.targetLanguage("pt"));
                    textViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            tvNome.setText(translationNome.getTranslatedText());
                            tvGenero.setText(translationGenero.getTranslatedText());
                            tvCorOlho.setText(translationCorOlho.getTranslatedText());
                            tvCorCabelo.setText(translationCorCabelo.getTranslatedText());
                            tvCorPele.setText(translationCorPele.getTranslatedText());
                        }
                    });

                }
                catch (Exception ex){
                    return ex.toString();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String msg) {
                progressDialog.dismiss();
                if (msg != null) {
                    if (!msg.isEmpty()) {
                        Toast.makeText(context, "Não foi possível realizar a tradução!"
                                , Toast.LENGTH_LONG).show();
                    }
                }
            }
        }.execute();

    }

}
