package com.developer.fbenutti.barsawars;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.developer.fbenutti.barsawars.modelo.Film;
import com.developer.fbenutti.barsawars.modelo.SWModelList;
import com.developer.fbenutti.barsawars.sw.StarWars;
import com.developer.fbenutti.barsawars.sw.StarWarsApi;
import com.developer.fbenutti.barsawars.util.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FilmesActivity extends AppCompatActivity {

    private Film f;
    private List<Film> Filmes;
    private ListView lvFilmes;
    Context context;
    String msg = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filmes);
        context = getApplicationContext();

        Filmes = new ArrayList<Film>();
        lvFilmes = (ListView) findViewById(R.id.lv);

//        StarWarsApi.init();
//        StarWars api = StarWarsApi.getApi();
//
//        api.getAllFilms(1, new Callback<SWModelList<Film>>() {
//
//            @Override
//            public void success(SWModelList<Film> filmSWModelList, Response response) {
//                Filmes = filmSWModelList.results;
//
//                if (Filmes != null) {
//                    if (Filmes.size() > 0) {
//                        FilmeListAdapter fla = new FilmeListAdapter(
//                                getApplicationContext(), Filmes);
//                        lvFilmes.setAdapter(fla);
//                    }
//                }
//            }
//
//            public void failure(RetrofitError error) {
//
//            }

        buscarFilmesBackground();

        //Seta o listener para a selecao com click simples
        lvFilmes.setOnItemClickListener(selecionarFilme);
    }


    private void buscarFilmesBackground() {

        new AsyncTask<Void, Void, String>() {
            private ProgressDialog progressDialog = new ProgressDialog(
                    FilmesActivity.this);

            @Override
            protected void onPreExecute() {
                progressDialog.setMessage("Procurando informações na galáxia mais próxima...");
                progressDialog.show();
                //Util.addMsg(context, "teste");
            }

            @Override
            protected String doInBackground(Void... params) {

                try {
                    StarWarsApi.init();
                    StarWars api = StarWarsApi.getApi();

                    api.getAllFilms(1, new Callback<SWModelList<Film>>() {

                        @Override
                        public void success(SWModelList<Film> filmSWModelList, Response response) {
                            Filmes = filmSWModelList.results;

                            if (Filmes != null) {
                                if (Filmes.size() > 0) {
                                    FilmeListAdapter fla = new FilmeListAdapter(
                                            getApplicationContext(), Filmes);
                                    lvFilmes.setAdapter(fla);
                                }
                            }
                        }

                        public void failure(RetrofitError error) {
                            String erro = "Ops! Ocorreu um erro ao tentar acessar as informações!";
                            msg = erro;
                        }
                    });
                }
//                catch (IOException ex) {
//                    String erro = "Ops, ocorreu um erro - IO";
//                    msg = erro;
//                }
                catch (Exception ex) {
                    String erro = "Ops, ocorreu um erro!";
                    msg = erro;
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                progressDialog.dismiss();
                if (!msg.isEmpty())
                {
                    Util.addMsg(context, msg + "\n");
                }

            }
        }.execute(null, null, null);
    }

    private AdapterView.OnItemClickListener selecionarFilme = new AdapterView.OnItemClickListener() {

        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) {
            f = Filmes.get(pos);
            Intent i = new Intent(arg1.getContext(), FilmeActivity.class);
            i.putExtra("filme", f);
            startActivity(i);
        }

    };

}
