package com.developer.fbenutti.barsawars;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Splash extends AppCompatActivity {
private TextView tv;
private TextView tv_titulo;
private ImageView iv;
private Thread timer;
MediaPlayer ThemeMP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        tv = (TextView) findViewById(R.id.tv);
        tv_titulo = (TextView) findViewById(R.id.tv_titulo);
        iv = (ImageView) findViewById(R.id.iv);
        Button btnSkip = (Button) this.findViewById(R.id.btnSkip);


        Typeface face = Typeface.createFromAsset(getAssets(),
                "GothamNights.otf");
        tv.setTypeface(face);
        tv_titulo.setTypeface(face);

        Animation myanim = AnimationUtils.loadAnimation(this, R.anim.mytransition);
        tv.startAnimation(myanim);
        tv_titulo.startAnimation(myanim);
        iv.startAnimation(myanim);

        ThemeMP = MediaPlayer.create(this, R.raw.theme);


        final Intent i = new Intent(this, MainActivity.class);

        timer = new Thread(){
            public void run (){
                try {
                    ThemeMP.start();
                    sleep(9000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    startActivity(i);
                    finish();
                }
            }
        };

        timer.start();

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timer.interrupt();
                ThemeMP.stop();
            }
        });

    }
}
