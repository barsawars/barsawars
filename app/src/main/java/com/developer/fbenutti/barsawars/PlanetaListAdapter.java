package com.developer.fbenutti.barsawars;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.developer.fbenutti.barsawars.modelo.Planet;

import java.util.List;

public class PlanetaListAdapter extends BaseAdapter {

	private Context context;
	private List<Planet> lista;

	public PlanetaListAdapter(Context context, List<Planet> planetas){
		this.context = context;
		this.lista = planetas;
	}
	
	@Override
	public int getCount() {
		return this.lista.size();
	}

	@Override
	public Object getItem(int posicao) {
		return this.lista.get(posicao);
	}

	@Override
	public long getItemId(int posicao) {
		return posicao;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Planet f = lista.get(position);
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.planetas, null);
		
		TextView nome = (TextView) view.findViewById(R.id.txtRespostaNome);
		nome.setText(f.getName());
		
		TextView genero = (TextView) view.findViewById(R.id.txtRespostaPopulacao);
		genero.setText(String.valueOf(f.getPopulation()));
		
		return view;
	}

}
